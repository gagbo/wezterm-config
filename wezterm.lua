local wezterm = require("wezterm")
local theme = require("theme")
local helix = require("helix")
local emacs = require("emacs")
local keys = require("keys")
local startup = require("startup")

local config = {}
if wezterm.config_builder then
    config = wezterm.config_builder()
end


local path = '/opt/homebrew/bin:' .. "/home/linuxbrew/.linuxbrew/bin:" .. os.getenv('PATH')
config.set_environment_variables = {
    PATH = path
}
wezterm.log_warn("Path is " .. path)

config.use_ime = false
if wezterm.run_child_process({ "which", "distrobox" }) then
-- if os.execute("command -v distrobox") then
    config.default_prog = { "distrobox", "enter", "fedora" }
else
    config.default_prog = { "fish", "-l" }
end

theme.apply(config)
-- helix.apply(config)
emacs.apply(config)
startup.apply(config)
keys.apply(config)

return config
