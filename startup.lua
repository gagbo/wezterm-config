local wezterm = require("wezterm")
local mux = wezterm.mux
local M = {}

function M.apply(config)
    wezterm.on("gui-startup", function(cmd)
        -- allow `wezterm start -- something` to affect what we spawn
        -- in our initial window
        local args = nil
        if cmd then
            args = cmd.args
        end

        -- Set a workspace for coding on a current project
        -- Top pane is for the editor, bottom pane is for the build tool
        local project_dir = wezterm.home_dir
        local tab, build_pane, window = mux.spawn_window({
            -- workspace = 'coding',
            cwd = project_dir,
            args = args,
        })
        local editor_pane = build_pane:split({
            direction = "Left",
            size = 0.6,
            cwd = project_dir,
        })

        build_pane:send_text("\n")
    end)
end

return M
