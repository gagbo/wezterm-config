local wezterm = require("wezterm")
local tab_ext = require("utils.tab_ext")
local pane_ext = require("utils.pane_ext")
local emacs = {}

local function line_col_emacsclient_args(line, col)
    if col then
        return "+" .. line .. ":" .. col
    elseif line then
        return "+" .. line
    else
        return ""
    end
end

local function opt_str_as_opt_number(str)
    if str then
        return str
    else
        return "nil"
    end
end

local function line_col_lisp_args(line, col)
    return opt_str_as_opt_number(line) .. " " .. opt_str_as_opt_number(col)
end

local function parse_file_uri(uri)
    local file, line, col

    local _, _, last_segment = uri:find(".*(:%d+)")
    if last_segment then
        uri = uri:sub(1, -1 - last_segment:len())
        local _, _, before_last_segment = uri:find(".*(:%d+)")

        if before_last_segment then
            uri = uri:sub(1, -1 - before_last_segment:len())
            col = last_segment:gsub(":", "")
            line = before_last_segment:gsub(":", "")
        else
            line = last_segment:gsub(":", "")
        end
    end

    file = uri
    return file, line, col
end

local function activate_link_handling(config)
    config.hyperlink_rules = wezterm.default_hyperlink_rules()

    table.insert(config.hyperlink_rules, {
        regex = "~?(/[^/ '\"`:]*)+(:%d+)?(:%d+)?",
        format = "em://$0",
    })

    wezterm.on("open-uri", function(window, pane, uri)
        local start, _ = uri:find("em://")
        if start == 1 then
            local path = uri:gsub("^em://", "")
            wezterm.log_info("Emacs link handling of: " .. path)
            local file, line, col = parse_file_uri(path)
            wezterm.log_info("Path parsed:", line, col, file)

            local tab = window:active_tab()
            local em_pane = tab_ext.get_program_pane(tab, "emacsclient")
            if em_pane then
                -- M-x eval-expression RET (gagbo-find-file path line col) RET
                em_pane.pane:send_text("\x1bxeval-expression\r\n")
                em_pane.pane:send_text('(gagbo-find-file "' .. file .. '" ' .. line_col_lisp_args(line, col) .. ")\r\n")

                em_pane.pane:activate()
            else
                pane_ext.create_program_pane(
                    pane,
                    { "emacsclient", "-nw", "-c", "-a", '""', line_col_emacsclient_args(line, col), file },
                    { top_level = true, direction = "Left" }
                )
            end
            return false
        end
    end)
end

function emacs.apply(config)
    activate_link_handling(config)
end

return emacs
