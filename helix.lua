local wezterm = require("wezterm")
local tab_ext = require("utils.tab_ext")
local pane_ext = require("utils.pane_ext")
local helix = {}

local function activate_link_handling(config)
    config.hyperlink_rules = wezterm.default_hyperlink_rules()

    table.insert(config.hyperlink_rules, {
        regex = "~?(/[^/ '\"`:]*)+(:[0-9]+)?(:[0-9]+)?",
        format = "hx://$0",
    })

    wezterm.on("open-uri", function(window, pane, uri)
        local start, _ = uri:find("hx://")
        if start == 1 then
            local path = uri:gsub("^hx://", "")

            local tab = window:active_tab()
            local hx_pane = tab_ext.get_program_pane(tab, "hx")
            if hx_pane then
                -- Escape, then :o path RET
                hx_pane.pane:send_text("\x1b\r")
                hx_pane.pane:send_text(":o " .. path .. "\r\n")

                hx_pane.pane:activate()
            else
                pane_ext.create_program_pane(pane, { "hx", path }, { top_level = true, direction = "Left" })
            end
            return false
        end
    end)
end

function helix.apply(config)
    activate_link_handling(config)
end

return helix
