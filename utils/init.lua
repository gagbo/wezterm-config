local wezterm = require("wezterm")

local utils = {}

function utils.is_apple()
    -- Using a . instead of - because I have no idea how to match
    -- properly on a literal -
    return not not wezterm.target_triple:match("^(.*)apple.darwin$")
end

return utils
