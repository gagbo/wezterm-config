local tab_ext = {}

--- Equivalent to POSIX basename(3)
--- Given "/foo/bar" returns "bar"
--- Given "c:\\foo\\bar" returns "bar"
---@param s string
---@return string, integer
local function basename(s)
    return string.gsub(s, "(.*[/\\])(.*)", "%2")
end

--- Look for a pane in the given tab running a program
---@param tab _.wezterm.MuxTab the tab to search in
---@param program string The program to search for
---@return _.wezterm.PaneWithInfo | nil
function tab_ext.get_program_pane(tab, program)
    local panes = tab:panes_with_info()

    for _, pane_info in ipairs(panes) do
        local info = pane_info.pane:get_foreground_process_info()
        if info then
            if basename(info.executable) == program then
                return pane_info
            end
        end
    end

    return nil
end

return tab_ext
