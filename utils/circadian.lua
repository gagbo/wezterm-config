--- Lustrous integration in Wezterm for Circadian behaviour

local lustrous = require("lib.lustrous")
local wezterm = require("wezterm")

local circadian = {}
circadian.time_changed_event = "circadian::time_changed"

---@class Date
---@field year number
---@field month number
---@field day number
---@field yday number
---@field wday number
---@field hour number
---@field min number
---@field sec number
---@field isdst boolean

---@alias Sun
---| '"day"' # It is day
---| '"night"'

---@class (exact) Args
---@field lat number # Latitude
---@field lon number # Longitude
---@field offset number | nil # Hours away from UTC, only useful if `date` is set.
---@field zenith number | nil
---@field date Date | nil
---@field current_time Sun | nil
---@field update_interval number # In seconds

--- Update the current situation of the sun
---@param args Args
function circadian.update_time(args)
    local new_time, _, _, time_left = lustrous.get_time(args)

    if args.current_time and args.current_time ~= new_time then
        wezterm.emit(circadian.time_changed_event, args.current_time)
    end

    args.current_time = new_time
    wezterm.time.call_after(math.min(time_left, args.update_interval), function()
        circadian.update_time(args)
    end)
end

--- Initialize Lustrous
---@param args Args
---@return Sun
function circadian.init(args)
    local current_time, _, _, time_left = lustrous.get_time(args)
    args.current_time = current_time

    wezterm.on(circadian.time_changed_event, function(
        time --[[@as Sun]]
    )
        wezterm.log_info("Time changed to " .. time)
        wezterm.reload_configuration()
        wezterm.log_info("Configuration reloaded")
    end)

    wezterm.time.call_after(math.min(args.update_interval, time_left), function()
        circadian.update_time(args)
    end)

    return current_time
end

return circadian
