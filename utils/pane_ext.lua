local pane_ext = {}

-- This alias is created from the generated signature in types/lua/wezterm
--- @alias PaneSplitOptions { args: string[], cwd: string; set_environment_variables: table<string, string>, domain: 'DefaultDomain' | { DomainName: string }, direction: 'Right' | 'Left' | 'Up' | 'Down', top_level: boolean, size: number }

--- Create a split from the pane with the given program
---@param pane _.wezterm.Pane The pane to act on
---@param program string | table | nil The program to run in the new tab (nil means default program)
---@param split_options nil | PaneSplitOptions The other options to pass to the split
function pane_ext.create_program_pane(pane, program, split_options)
    local split_args = split_options or {}

    if type(program) == "table" then
        split_args.args = program
    elseif type(program) == "string" then
        split_args.args = { program }
    end

    pane:split(split_args)
end

return pane_ext
