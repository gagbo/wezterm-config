local wezterm = require("wezterm")
local projects = {}

local function project_dirs()
    -- Start with your home directory as a project, 'cause you might want
    -- to jump straight to it sometimes.
    local project_list = { wezterm.home_dir }

    -- Check if fd exists, and launch `cmd` in a shell
    local fd_exists = wezterm.run_child_process({"which", "fd"})
    if fd_exists then
        local cmd = {"fd", "-u", "-tdirectory", "--exclude", "'\\.local'", "--exclude", "'\\.cargo'", "^\\.git$'", "~", "-x", "echo", "{//}"}
        local handle, result = wezterm.run_child_process(cmd)
        if handle then
            for str in string.gmatch(result, "([^%s]+)") do
                table.insert(project_list, str)
            end
        end
    end

    return project_list
end

function projects.choose_project()
    local choices = {}
    for _, value in ipairs(project_dirs()) do
        table.insert(choices, { label = value })
    end

    return wezterm.action.InputSelector({
        title = "Projects",
        choices = choices,
        fuzzy = true,
        action = wezterm.action_callback(function(child_window, child_pane, id, project_dir)
            -- "project_dir" may be empty if nothing was selected. Don't bother doing anything
            -- when that happens.
            if not project_dir then
                return
            end

            -- The SwitchToWorkspace action will switch us to a workspace if it already exists,
            -- otherwise it will create it for us.
            child_window:perform_action(
                wezterm.action.SwitchToWorkspace({
                    -- We'll give our new workspace a nice name, like the last path segment
                    -- of the directory we're opening up.
                    name = project_dir:match("([^/]+)$"),
                    -- Here's the meat. We'll spawn a new terminal with the current working
                    -- directory set to the directory that was picked.
                    spawn = { cwd = project_dir },
                }),
                child_pane
            )
        end),
    })
end

return projects
