local wezterm = require("wezterm")
local utils = require("utils")
local circadian = require("utils.circadian")

local M = {}
local font
local font_size
local color_scheme
local daylight = circadian.init({
    lat = 48.8534,
    lon = 2.3488,
    update_interval = 600,
})

if utils.is_apple() then
    font = wezterm.font("FantasqueSansM Nerd Font")
    font_size = 18.0
    if daylight == "day" then
        color_scheme = "Alabaster"
    else
        color_scheme = "Helios (base16)"
    end
else
    font = wezterm.font("ZedMono Nerd Font")
    font_size = 14.0
    if daylight == "day" then
        color_scheme = "Helios (base16)"
    else
        color_scheme = "Helios (base16)"
    end
end

local function basename(s)
    -- Nothing a little regex can't fix
    return string.gsub(s, "(.*[/\\])(.*)", "%2")
end

---@param pane _.wezterm.Pane
local function get_cwd(pane)
    local cwd = pane:get_current_working_dir()
    if cwd then
        if type(cwd) == "userdata" then
            -- Wezterm introduced the URL object in 20240127-113634-bbcac864
            return basename(cwd.file_path)
        end
        -- 20230712-072601-f4abf8fd or earlier version
        return basename(cwd)
    end
    return ""
end

---@param window _.wezterm.Window
local function get_left_segments(window)
    local stat = window:active_workspace() or ""
    -- It's a little silly to have workspace name all the time
    -- Utilize this to display LDR or current key table name
    if window:active_key_table() then
        stat = window:active_key_table()
    end
    if window:leader_is_active() then
        stat = "LDR"
    end

    return {
        wezterm.nerdfonts.oct_table .. " " .. stat,
    }
end

---@param window _.wezterm.Window
local function get_right_segments(window, pane)
    local cwd = get_cwd(pane)
    local time = wezterm.strftime("%H:%M")
    local cmd = pane:get_foreground_process_name()
    -- CWD and CMD could be nil (e.g. viewing log using Ctrl-Alt-l)
    cmd = cmd and basename(cmd) or ""

    return {
        wezterm.nerdfonts.md_folder .. "  " .. cwd,
        wezterm.nerdfonts.fa_code .. " " .. cmd,
        wezterm.nerdfonts.md_clock .. " " .. time,
    }
end

local function get_gradient(segments, scheme, direction)
    local bg = wezterm.color.parse(scheme.background)
    local black = wezterm.color.parse("black")
    local white = wezterm.color.parse("white")
    local is_dark_scheme = bg:contrast_ratio(white) > bg:contrast_ratio(black)

    -- Each powerline segment is going to be coloured progressively
    -- darker/lighter depending on whether we're on a dark/light colour
    -- scheme. Let's establish the "from" and "to" bounds of our gradient.
    local gradient_to, gradient_from = bg, bg
    if is_dark_scheme then
        gradient_from = gradient_to:lighten(0.2)
    else
        gradient_from = gradient_to:darken(0.2)
    end

    -- Yes, WezTerm supports creating gradients, because why not?! Although
    -- they'd usually be used for setting high fidelity gradients on your terminal's
    -- background, we'll use them here to give us a sample of the powerline segment
    -- colours we need.
    local gradient = wezterm.color.gradient(
        {
            orientation = "Horizontal",
            colors = direction == "right" and { gradient_from, gradient_to } or { gradient_to, gradient_from },
        },
        #segments -- only gives us as many colours as we have segments.
    )

    return gradient
end

--- Status Update handler
---@param window _.wezterm.Window
---@param pane _.wezterm.Pane
local function status_update(window, pane)
    local SOLID_LEFT_ARROW = wezterm.nerdfonts.pl_right_hard_divider
    local SOLID_RIGHT_ARROW = wezterm.nerdfonts.pl_left_hard_divider

    local scheme = window:effective_config().resolved_palette
    local fg = wezterm.color.parse(scheme.foreground)

    local left_segments = get_left_segments(window)
    local right_segments = get_right_segments(window, pane)
    local left_gradient = get_gradient(left_segments, scheme, "left")
    local right_gradient = get_gradient(right_segments, scheme, "right")

    local left_elements = {}
    -- We'll build up the elements to send to wezterm.format in this table.
    local right_elements = {}

    for i, seg in ipairs(right_segments) do
        local is_first = i == 1

        if is_first then
            table.insert(right_elements, { Background = { Color = "none" } })
        end
        table.insert(right_elements, { Foreground = { Color = right_gradient[i] } })
        table.insert(right_elements, { Text = SOLID_LEFT_ARROW })

        table.insert(right_elements, { Foreground = { Color = fg } })
        table.insert(right_elements, { Background = { Color = right_gradient[i] } })
        table.insert(right_elements, { Text = " " .. seg .. " " })
    end

    window:set_right_status(wezterm.format(right_elements))

    for i, seg in ipairs(left_segments) do
        local is_last = i == #left_segments

        table.insert(left_elements, { Foreground = { Color = fg } })
        table.insert(left_elements, { Background = { Color = left_gradient[i] } })
        table.insert(left_elements, { Text = " " .. seg .. " " })

        table.insert(left_elements, { Foreground = { Color = left_gradient[i] } })
        if is_last then
            table.insert(left_elements, { Background = { Color = "none" } })
        end
        table.insert(left_elements, { Text = SOLID_RIGHT_ARROW })
    end

    window:set_left_status(wezterm.format(left_elements))
end

-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
function tab_title(tab_info)
    local title = tab_info.tab_title
    -- if the tab title is explicitly set, take that
    if title and #title > 0 then
        return title
    end
    -- Otherwise, use the title from the active pane
    -- in that tab
    return tab_info.active_pane.title
end

--- Return a new configuration object with theming applied
function M.apply(config)
    config.font = font
    config.font_size = font_size
    config.color_scheme = color_scheme
    config.window_background_opacity = 0.8
    config.status_update_interval = 500

    -- Dim inactive panes
    config.inactive_pane_hsb = {
        saturation = 0.50,
        brightness = 0.75,
    }

    wezterm.on("update-status", status_update)
end

return M
